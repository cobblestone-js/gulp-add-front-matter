gulp-add-front-matter
=====================

> A *gulp* plugin for inserting a data object back into a post.

## Setup and usage

Install `gulp-add-front-matter` using `npm`:

```sh
npm i gulp-add-front-matter
```

In your `gulpfile.js`:

```js
var gulp = require('gulp'),
    addFrontMatter = require('gulp-add-front-matter'),
	grayMatter = require('gulp-gray-matter');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(grayMatter({ property: "data" }))
	// Manipulate the data object.
    .pipe(addFrontMatter({ property: "data" })
    .pipe(gulp.dest('./dest'));
});
```

A common use for this plugin is process various Markdown files, manipulate their front matter, and write out the results.

## Options

### prefix

*string*

Default: `---\n`

The prefix characters to write out before the YAML data.

### property

*string*

Default: `data`

The name of the property in the resulting object that needs to be written out.

### suffix

*string*

Default: `---\n`

The text to write after the YAML data.
