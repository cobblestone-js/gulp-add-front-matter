var through = require('through2');
var yaml = require('js-yaml');

module.exports = function(options) {
    // Set the default properties, if missing.
    options = options || {};
    options.property = options.property || 'data';
    options.prefix = options.prefix || '---\n';
    options.suffix = options.suffix || '---\n';

    // Create the pipe and pass it on.
    var setPipe = through.obj(
        function(file, encoding, callback) {
            // If we don't have the property, then we don't do anything.
            if (!file[options.property]) {
                return callback(null, file);
            }

            // Otherwise, inject the YAML header.
            var header = yaml.dump(file[options.property]);
            file.contents = new Buffer(
				options.prefix
					+ header
					+ options.suffix
					+ String(file.contents)
                                        + "\n");
            callback(null, file);
        });

    return setPipe;
};
