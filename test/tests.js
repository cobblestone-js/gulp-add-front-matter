'use strict';

var expect = require('expect');
var streamify = require('stream-array');
var plugin = require('../lib/index');
var File = require('vinyl');

describe('gulp-add-front-matter', function() {
    it('basic writing', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: {
                title: "Title"
            },
			contents: new Buffer('File contents.'),
            path: 'bob.html'
        });

        // Run the plugin through the stream and look at the output.
        var files = [];

        streamify([fakeFile])
            .pipe(plugin({}))
            .on('data', function(file) {
                expect(file.path)
                    .toEqual(
                        'bob.html',
                        'file.path was not expected');

                expect(file.contents.toString())
                    .toEqual(
                        "---\ntitle: Title\n---\nFile contents.");

                done();
            });
    });
});
